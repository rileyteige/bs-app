module.exports = function (grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');

	grunt.initConfig({
		'pkg': grunt.file.readJSON('package.json'),

		'concat': {
			'dist': {
				'src': ['public/javascripts/**/*.js', 'js/node_modules/shared/**/*.js'],
				'dest': 'dist/<%= pkg.name %>-<%= pkg.version %>.concat.js'
			}
		},

		'babel': {
			options: {
				sourceMap: true,
				presets: ['es2015']
			},
			dist: {
				files: {
					'dist/<%= pkg.name %>-<%= pkg.version %>.js': 'dist/<%= pkg.name %>-<%= pkg.version %>.concat.js'
				}
			}
		},

		'clean': ['dist'],

		'uglify': {
			'options': {
				'mangle': true
			},
			'dist': {
				'files': {
					'dist/<%= pkg.name %>-<%= pkg.version %>.min.js': ['dist/<%= pkg.name %>-<%= pkg.version %>.js']
				}
			}
		}
	});

	grunt.registerTask('build', 
		[
			'concat',
			'babel',
			'uglify'
		]);

};