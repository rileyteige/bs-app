'use strict';

var express = require('express');
var router = express.Router();

router.get(/^\/(.{1,64})$/, (req, res) => {
	const name = req.params[0];
	res.render(`partials/${name}`);
})

module.exports = router;
