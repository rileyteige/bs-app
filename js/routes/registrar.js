const index     = require('./index');
const partials  = require('./partials');

module.exports = {
	registerRoutes: function (app) {
		app.use('/', index);
		app.use('/partials', partials);
	}
}