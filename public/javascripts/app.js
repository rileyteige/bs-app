/**
 * Setup of main Angular JS application.
 *
 * @see controllers
 * @see services
 */

const appName = 'ExampleApp';
const app = {
	name: appName,
	services: `${appName}.srv`,
	controllers: `${appName}.ctrl`,
	models: `${appName}.model`,
	directives: `${appName}.directives`
}

angular.module(`${app.name}`,
	[
		`${app.controllers}`,
		`${app.services}`,
		`${app.models}`,
		`${app.directives}`
	]
);