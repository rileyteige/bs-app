/**
 * This is an example controller.
 * It triggers the angularCheckService and puts the returned value on the scope.
 *
 * @see services
 */
 angular.module(`${app.controllers}`, [`${app.services}`, `${app.models}`]);