 angular.module(`${app.controllers}`)

 .controller('FlightLogController', ['$scope', 'flightLogService', 'aircraftService', 'FlightLogEntry', 'Airport', function ($scope, flightLogService, aircraftService, FlightLogEntry, Airport) {
 	const logSum = log => {
 		const sum = new FlightLogEntry();

 		log.forEach(entry => {
 			sum.breakdown.airplane.sel   += entry.breakdown.airplane.sel;
 			sum.breakdown.airplane.mel   += entry.breakdown.airplane.mel;
 			sum.breakdown.airplane.ses   += entry.breakdown.airplane.ses;
 			sum.breakdown.airplane.mes   += entry.breakdown.airplane.mes;
 			sum.breakdown.groundTrainer  += entry.breakdown.groundTrainer;
 			sum.breakdown.dualReceived   += entry.breakdown.dualReceived;
 			sum.breakdown.pilotInCommand += entry.breakdown.pilotInCommand;
 			sum.breakdown.day            += entry.breakdown.day;
 			sum.breakdown.night          += entry.breakdown.night;
 			sum.breakdown.crossCountry        += entry.breakdown.crossCountry;
 			sum.breakdown.actualInstrument    += entry.breakdown.actualInstrument;
 			sum.breakdown.simulatedInstrument += entry.breakdown.simulatedInstrument;
 			sum.breakdown.totalDuration       += entry.breakdown.totalDuration;
 			entry.instrumentApproaches.forEach(approach => sum.instrumentApproaches.push(approach));
 			sum.landings.day   += entry.landings.day;
 			sum.landings.night += entry.landings.night;
 		})

 		return sum;
 	}

 	$scope.approachNames = function (entry) {
 		return entry.instrumentApproaches.map(function (appr) {
 			return appr.approach + ' @ ' + appr.airport;
 		}).join(", ");
 	}

 	$scope.newEntry = new FlightLogEntry();
 	$scope.selectedAircraft = {};

 	$scope.addLogEntry = function () {
 		let newEntry = $scope.newEntry;
 		let landings = newEntry.landings.dayNight.split('/');
 		newEntry.departurePoint = new Airport(newEntry.departurePoint);
 		newEntry.landings.day = parseInt(landings[0]);
 		newEntry.landings.night = parseInt(landings[1]);
 		newEntry.aircraft = $scope.selectedAircraft;
 		flightLogService.addFlightLogEntry(newEntry).then(id => {
 			newEntry.id = id;
 			$scope.log.push(newEntry);
 			$scope.sum = logSum([ $scope.sum, newEntry ]);
 			$scope.newEntry = new FlightLogEntry();
 		})
 	}

    aircraftService.getAircraft().then(aircraft => { $scope.aircraft = aircraft; if (aircraft.length) { $scope.selectedAircraft = aircraft[0]; } });
 	flightLogService.getFlightLog().then(log => {
 		$scope.log = log;
 		$scope.sum = logSum(log);
 	})
 }]);