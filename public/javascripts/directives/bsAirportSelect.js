angular.module(`${app.directives}`)

.directive('bsAirportSelect', ['AirportLocator', (AirportLocator) => ({
	templateUrl: 'partials/directives/airportSelect',
	replace: true,
	scope: {
		'ngModel': '=bsModel'
	},
	controller: ['$scope', '$timeout', ($scope, $timeout) => {
		$scope.itemArray = [
		    {id: 1, name: 'first'},
		    {id: 2, name: 'second'},
		    {id: 3, name: 'third'},
		    {id: 4, name: 'fourth'},
		    {id: 5, name: 'fifth'},
		];

		$scope.selectedItem = $scope.itemArray[0];
	}],
	link: ($scope, $element) => {
		if (typeof $scope.ngModel !== 'array') $scope.ngModel = [];

		const locator = new AirportLocator();

		$($element).select2({
			multiple: true,
			placeholder: 'KIXD,KICT',
			ajax: {
				url: (params) => locator.getUrl(params.term || ''),
				dataType: 'json',
				data: (params) => ({
					q: params.term,
					page: params.page
				}),
				processResults: (data, params) => {
					params.page = params.page || 1;
					return {
						results: $.map(data, (obj) => {
							obj.id = obj.icao;
							obj.text = obj.icao;
							return obj;
						})
					}
				}
			},
			escapeMarkup: (markup) => markup,
			id: (airport) => `${airport.icao}`,
			templateResult: (airport) => `<div class="clearfix">${airport.icao}</div>`,
			templateSelection: (airport) => `${airport.text}`
		});

		$($element).on('select2:unselect', function (e) {
			for (let i = 0; i < $scope.ngModel.length; i++) {
				if ($scope.ngModel[i].icao == e.params.data.icao) {
					$scope.ngModel.splice(i, 1);
				}
			}

			console.log($(this).val(), $(this).select2('data'), $scope.ngModel);
		});

		$($element).on('select2:select', function (e) {
			$scope.ngModel.push(angular.copy(e.params.data));
			console.log(e.params.data, $(this).val(), $(this).select2('data'), $scope.ngModel);
		});
	}
})]);