angular.module(`${app.models}`,
	[
		'shared.models.flightLog',
		'shared.models.airport'
	]);