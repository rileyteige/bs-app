angular.module(`${app.services}`)

.factory('aircraftService', ['ajaxService', 'AircraftLocator', '$q', function (ajaxService, AircraftLocator, $q) {
	const locator = new AircraftLocator();

	const getAircraft = function () {
		const request = {
			url: locator.getUrl()
		};

		var key = 'aircraft';
		return sessionStorage[key]
			? $q.resolve(angular.fromJson(sessionStorage[key]))
			: ajaxService.get(request).then(aircraft => {
				sessionStorage[key] = angular.toJson(aircraft);
				return aircraft;
			});
	}

    return {
    	getAircraft
    };
}]);