angular.module(`${app.services}`)

/**
 * Simple wrapper for Angular's $http service, used to simplify calling conventions.
 */
.factory('ajaxService', ['$http', ($http) => {

	const requestor = (httpMethod) => {
		return (request) => {
			if (!request.url) throw new Error("Need a URL.");
			return httpMethod(request.url, request).then((result) => {
				return result.data;
			});
		};
	};

	return {
		get: requestor($http.get),
		post: requestor($http.post)
	};
}]);