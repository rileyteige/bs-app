angular.module(`${app.services}`)

.factory('flightLogService', ['ajaxService', 'FlightLogLocator', function (ajaxService, FlightLogLocator) {
	const locator = new FlightLogLocator();

	const getFlightLog = function () {
		const request = {
			url: locator.getUrl()
		};

		return ajaxService.get(request);
	}

	const addFlightLogEntry = function (entry) {
		const request = {
			url: locator.getUrl(),
			data: entry
		};

		return ajaxService.post(request);
	}

    return {
    	getFlightLog,
    	addFlightLogEntry
    };
}]);