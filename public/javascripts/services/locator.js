angular.module(`${app.services}`)

.factory('Locator', () => {
	const rootForNow = 'http://localhost:8080/api';

	class Locator {
		constructor(endpoint) {
			this.endpoint = endpoint || '';
		}

		getUrl(path = '') {
			return `${rootForNow}/${this.endpoint}${(path && path.indexOf('/') == 0) ? path : (path ? ('/' + path) : '')}`;
		}
	}

	return Locator;
})

.factory('FlightLogLocator', ['Locator', (Locator) => {
	class FlightLogLocator extends Locator {
		constructor() { super('flightLog'); }
	}

	return FlightLogLocator;
}])

.factory('AircraftLocator', ['Locator', (Locator) => {
	class AircraftLocator extends Locator {
		constructor() { super('aircraft'); }
	}

	return AircraftLocator;
}])

.factory('AirportLocator', ['Locator', (Locator) => {
	class AirportLocator extends Locator {
		constructor() { super('airport'); }
	}

	return AirportLocator;
}]);